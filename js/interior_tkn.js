$(function () {
    /*// jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });


    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top'
    })*/
    $('.navbar-submobi').on('show.bs.collapse', function () {
        var actives = $(this).find('.collapse.in'),
            hasData;

        if (actives && actives.length) {
            hasData = actives.data('collapse')
            if (hasData && hasData.transitioning) return
            actives.collapse('hide')
            hasData || actives.data('collapse', null)
        }
    });
    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function () {
        $('.navbar-toggle:visible').click();
    });


    $('.navbar-toggle:visible').click(function () {
        var btnMenu = $("#btnMenuMobile"),
            navDefault = $(".navbar-default");
        if ($("#navbarMobile").hasClass("in")) {
            btnMenu.css("color", "#3fa0cd");
            navDefault.removeClass("narbar-mobile").addClass("narbar-desk");
            btnMenu.find("i").removeClass("fa-close").addClass("fa-bars");
            $(".mobi-logo").toggleClass('desk-element mobi-element');
        } else {
            btnMenu.css("color", "#fff");
            btnMenu.find("i").removeClass("fa-bars").addClass("fa-close");
            navDefault.removeClass("narbar-desk").addClass("narbar-mobile");
            $(".mobi-logo").toggleClass('desk-element mobi-element');
        }

    });

    //Collapse and expand group contents
    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')           
            .toggleClass('show-heading, hide-heading');
    }
    
    $('.panel-collapse').on('hidden.bs.collapse', toggleChevron);
    $('.panel-collapse').on('shown.bs.collapse', toggleChevron);
    //Add text to heading mobile when Sub Menu Box is clicked
    $(".menu-box").bind( "click", function() {
          var textMenuBox = $(this).find("p").text();
          $(".subpage-brand").text(textMenuBox);
    });
    
});