$(function () {
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // handle the mouseenter functionality
    $("#effect-news .img-news").mouseenter(function () {
        $(this).addClass("hover");
    });
    // handle the mouseleave functionality
    $("#effect-news .img-news").mouseleave(function () {
        $(this).removeClass("hover");
    });

   
    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top'
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function () {
        $('.navbar-toggle:visible').click();
    });
    
   
    $('.navbar-toggle:visible').click(function () {
        var btnMenu = $("#btnMenuMobile"),
            navDefault = $(".navbar-default");
       if($("#navbarMobile").hasClass("in")){
          btnMenu.css("color","#3fa0cd");
           navDefault.removeClass("narbar-mobile");
           btnMenu.find("i").removeClass("fa-close").addClass("fa-bars");
           
       }else{
            btnMenu.css("color","#fff");
           btnMenu.find("i").removeClass("fa-bars").addClass("fa-close");
           navDefault.addClass("narbar-mobile");
           
        }     
       
    });
    
    var elLoiich = $("#loiich");
    var elVidu = $("#info_share");
    var osLoiich = elLoiich.offset().top;
    var osVidu = elVidu.offset().top;
    
    $( "#more_info_video" ).hide();
    $( "#more_info_text" ).hide();
    
    $(window).scroll(function() {
        var window_top = $(window).scrollTop(); 
        var window_width= $( window ).width();
        var position_top = $('#page-top').offset().top;
        if(window_width > 1170){

                if (window_top > position_top) {
                    $('.navbar-nav, #navActions').hide();
                } else {
                    $('.navbar-nav, #navActions').show();
                }
        }
        
        if (osLoiich <= window_top) {
            //Counter to count-title up to a target number
            $('.count-title').each(function () {
                var dataFrom = $(this).attr("data-from"),
                    dataTo = $(this).attr("data-to"),
                    percent = "%";
                    
                var duration = 2000;
                if(parseInt(dataTo) < 10){
                  duration = 2500;
                }
                $(this).prop('Counter', 0).animate({
                    Counter: dataTo
                }, {
                    duration: duration,
                    easing: 'swing',
                    step: function (now) {
                        if (dataFrom) {
                            if(dataFrom >= now){
                              $(this).text(Math.ceil(now) + "-" + Math.ceil(now) + percent);
                            }else{
                              $(this).text(dataFrom + "-" + Math.ceil(now) + percent);
                            }
                        } else if (dataTo.indexOf(".") > 0) {
                            $(this).text(now.toFixed(1) + percent);
                        } else {
                            $(this).text(Math.ceil(now) + percent);
                        }

                    }
                });
            });
        }
        
        if (osVidu <= window_top) {
          var height = $( "#more_info_text" ).height();
          var height_h = $( "#more_info_video" ).height();
          $( "#more_info_text" ).height(height);
          $( "#more_info_video" ).height(height_h);
          
          $( "#more_info_text" ).show("slide", { direction: "left" }, 1800);
          $("#more_info_video").delay(2000).show("slide", { direction: "left" }, 1800);
          
        }
    });
    
    $('.btn-send-phone1').on("click", function(e){
        var phone = $("#phone1").val(); 
        send_phone_number(phone);
    });
    $('.btn-send-phone2').on("click", function(e){
        var phone = $("#phone2").val();
        send_phone_number(phone);
    });
    
    function send_phone_number(phone){
        if(phone !== ""){
          window.open("http://app.tietkiemnhom.com/r/" + phone, '_blank');
        }
    }
    
    $(".flip-item").flip({
      axis: 'y',
      trigger: 'hover',
      autoSize: false
    });
    
    var current_item = "";
    //Call the carousel slides
    $('#carousel-b').bind('slide.bs.carousel', function (e) {
      if(current_item!==""){
       $('#carousel-a').carousel(current_item);
       current_item = "";
      }else{
        $('#carousel-a').carousel($('#carousel-b .item.active').data('slide-item'));
      }
    });
    $('#carousel-b').bind('slid', function (e) {
      if(current_item!==""){
       $('#carousel-a').carousel(current_item);
       current_item = "";
      }else{
        $('#carousel-a').carousel($('#carousel-b .item.active').data('slide-item'));
      }
    });
    
    $('#carousel-b').on('click', '.dotShare[data-slide-to]', function (ev) {
        $('#carousel-b').carousel('pause');
        current_item = $(this).data('slide-to');
        $('#carousel-b').carousel($(this).data('slide-to'));
	});
    
    $('.news-tkn').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        mouseDrag: true,
        touchDrag: true,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:2
            },
            800:{
                items:3
            }
        }
    });
});